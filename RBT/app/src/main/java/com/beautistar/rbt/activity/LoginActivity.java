package com.beautistar.rbt.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.utils.GpsService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends CommonActivity {

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_userPass) EditText edt_userPass;
    @BindView(R.id.btn_login) Button btn_login;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        Typeface font = Typeface.createFromAsset(getAssets(), "font/Gobold Regular.otf");
        btn_login.setTypeface(font);

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.login);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);
                return false;
            }
        });


    }

    @OnClick(R.id.btn_login) void gotoLogin(){

        if (!checkValid()) return;

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;
        Log.d("url==>", url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString().trim());
                    params.put(ReqConst.PARAM_PASSWORD, edt_userPass.getText().toString());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseLogin(String json){

        closeProgress();

        Log.d("Response==login>", json);

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                JSONObject user_info = response.getJSONObject(ReqConst.RES_USER_INFO);

                Commons.g_user.setUser_id(user_info.getInt(ReqConst.RES_USER_ID));
                Commons.g_user.setFull_name(user_info.getString(ReqConst.RES_FULL_NAME));

                EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL, edt_email.getText().toString().trim()).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, edt_userPass.getText().toString().trim()).save();

                startActivity(new Intent(this, MainActivity.class));
                overridePendingTransition(0,0);
                finish();

            } else showAlertDialog(response.getString(ReqConst.RES_MSG));

        } catch (JSONException e) {
            e.printStackTrace();
            closeProgress();
        }
    }


    @OnClick(R.id.txv_back) void createNewAccount(){

        startActivity(new Intent(this, SignUpActivity.class));
        overridePendingTransition(0,0);
        finish();

    }

    private boolean checkValid(){

        if (edt_email.getText().length() == 0){
            showAlertDialog("Please input your email");
            return false;
        } else if (edt_userPass.getText().length() == 0) {
            showAlertDialog("Please input your password");
            return false;
        }

        return true;
    }
}
