package com.beautistar.rbt.utils;

import android.Manifest;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.activity.ShowDetailsActivity;
import com.beautistar.rbt.adapter.AlertShowListViewAdapter;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.model.AlertModel;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.preference.Preference;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.beautistar.rbt.common.Commons.cur_sub_location;

public class GpsService extends Service {

    final Handler mHandler = new Handler();
    Timer mTimer = new Timer();
    String currenttime="00-00-00 00:00:00";

    int wifistringth=0;
    int cellularstrength=0;
    int gpsstatus=0; // 0: turn on, 1: turn off

    public LocationManager myLocationManager;
    public boolean w_bGpsEnabled, w_bNetworkEnabled;

    public static double w_fLatitude = 0;
    public static double w_fLongitude = 0;
    public static double w_fspeed = 0;

    public static int MY_PERMISSION_LOCATION = 123;


    public static final int REQUEST_CODE = 0;
    public static DevicePolicyManager mDPM;
    public static ComponentName mAdminName;
    public static AudioManager AUDIOMANAGER;

    public static boolean isCallEnabled = true;
    public static boolean isSilenceEnabled=true;

    ArrayList<AlertModel> alertMap = new ArrayList<>();

    String idList = "";
    int getCount = 0;

    int badgeCnt = 0;

    public GpsService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("Gps Info Service", "Service start to run.");


        //AUDIOMANAGER= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

        // Initiate DevicePolicyManager.
        //mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        // Set DeviceAdminDemo Receiver for active the component with different option
        //mAdminName = new ComponentName(this, DeviceAdminDemo.class);

        // scheduling the current position updating task (Asynchronous)
        mTimer.schedule(doAsynchronousTask, 0, Constants.LOCATION_UPDATE_TIME);

        return START_STICKY; //super.onStartCommand(intent, flags, startId);
    }

    TimerTask doAsynchronousTask = new TimerTask() {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    Wifichecked();
                    //getTimestring();
                    gpsstatus=1; // turnon

                    final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        if(Constants.mainActivity!=null){
                            gpsstatus=0;  // turn off
                        }

                        // Call your Alert message
                    }else if(w_fLongitude==0.0 && w_fLatitude==0.0){

                        gpsstatus=0;

                    }else gpsstatus = 1;

                    // initLocationListener();
                    Constants.service_status = 1;
                    // send receivedLocation to server

                    Commons.CUR_LNG = w_fLongitude;
                    Commons.CUR_LAT = w_fLatitude;

                    Commons.STR_LNG = String.valueOf(w_fLongitude);
                    Commons.STR_LAT = String.valueOf(w_fLatitude);

                    initLocationListener();
                    findAlertMap();
                    getAlertList();

                }
            });
        }
    };

    //in 50k alerts
    private void getAlertList(){

        if(Commons.CUR_LAT == 0.0 && Commons.CUR_LNG == 0.0){
            //Toast.makeText(this, "Searching location...", Toast.LENGTH_SHORT).show();
            return;
        }
        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_ALERT_LIST;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetList(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));

                } catch (Exception e) {}
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetList(String json){

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code ==  ReqConst.CODE_SUCCESS){

                JSONArray alertList = response.getJSONArray(ReqConst.RES_ALERT_LIST);

                for (int i = 0; i < alertList.length (); i++ ){

                    JSONObject jsonAlert = (JSONObject) alertList.get(i);
                    AlertModel alertModel = new AlertModel();

                    alertModel.setId(jsonAlert.getInt(ReqConst.RES_ALERT_ID));
                    //compare the alert ID
                    String alertID = Preference.getInstance().getValue(this, PrefConst.PREFKEY_ALERTID, "");
                    if (alertID.contains(String.valueOf(alertModel.getId()))) continue;

                    alertModel.setFullName(jsonAlert.getString(ReqConst.RES_FULL_NAME));
                    alertModel.setAlertType(jsonAlert.getInt(ReqConst.RES_ALERT_TYPE));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_LOCATION));
                    alertModel.setSub_location(jsonAlert.getString(ReqConst.RES_SUB_LOCATION));
                    alertModel.setLatitude(jsonAlert.getDouble(ReqConst.RES_LATITUDE));
                    alertModel.setLongitude(jsonAlert.getDouble(ReqConst.RES_LONGITUDE));
                    alertModel.setMember(jsonAlert.getInt(ReqConst.RES_MEMBERS));
                    alertModel.setRecordTime(jsonAlert.getString(ReqConst.RES_RECORD_TIME));
                    alertModel.setDistance(jsonAlert.getDouble(ReqConst.RES_DISTANCE));

                    alertMap.add(alertModel);
                }

                Constants.CHANGE_MAP = alertMap;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //in 2k alerts
    private void findAlertMap(){

        if(Commons.CUR_LAT == 0.0 && Commons.CUR_LNG == 0.0){
            //Toast.makeText(this, "Searching location...", Toast.LENGTH_SHORT).show();
            return;
        }
        //showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_FIND_ALERT_MAP;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parsefindAlertMap(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    //params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));

                } catch (Exception e) {}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parsefindAlertMap(String json){

        try {
            JSONObject response = new JSONObject(json);
            int alertCount = response.getInt(ReqConst.RES_ALERT_COUNT);

            //get alert count on app icon
            if (alertCount != 0 && alertCount != badgeCnt){
                badgeCnt = alertCount;
                ShortcutBadger.applyCount(this, badgeCnt);
            } else if (alertCount == 0){
                badgeCnt = 0;
                ShortcutBadger.applyCount(this, 0);
            }

            int result_code = response.getInt(ReqConst.RES_CODE);
            //response app
            if (result_code ==  ReqConst.CODE_SUCCESS){

                JSONArray alertList = response.getJSONArray(ReqConst.RES_ALERT_LIST);

                if (alertList.length() == 0) Constants.CHANGE_MAP.clear();

                for (int i = 0; i < alertList.length(); i++ ){

                    JSONObject jsonAlert = (JSONObject) alertList.get(i);
                    AlertModel alertModel = new AlertModel();

                    alertModel.setId(jsonAlert.getInt(ReqConst.RES_ALERT_ID));
                    alertModel.setFullName(jsonAlert.getString(ReqConst.RES_FULL_NAME));
                    alertModel.setAlertType(jsonAlert.getInt(ReqConst.RES_ALERT_TYPE));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_LOCATION));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_SUB_LOCATION));
                    alertModel.setLatitude(jsonAlert.getDouble(ReqConst.RES_LATITUDE));
                    alertModel.setLongitude(jsonAlert.getDouble(ReqConst.RES_LONGITUDE));
                    alertModel.setMember(jsonAlert.getInt(ReqConst.RES_MEMBERS));
                    alertModel.setRecordTime(jsonAlert.getString(ReqConst.RES_RECORD_TIME));
                    alertModel.setDistance(jsonAlert.getDouble(ReqConst.RES_DISTANCE));

                    //alertMap.add(alertModel);

                    //if (idList.length() == 0)Preference.getInstance().put(this, PrefConst.PREFKEY_MAPID, String.valueOf(Commons.g_user.getUser_id()));
                    idList = Preference.getInstance().getValue(this, PrefConst.PREFKEY_MAPID, "");

                    if (!idList.contains(String.valueOf(alertModel.getId()))){

                            try {
                                //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                Uri notification = Uri.parse("android.resource://com.beautistar.rbt/raw/ic_siren");
                                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                r.play();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        if (idList.length() == 0) {
                            Preference.getInstance().put(this, PrefConst.PREFKEY_MAPID, String.valueOf(alertModel.getId()));
                        }

                        else {

                            idList = idList + "," + alertModel.getId();
                            Preference.getInstance().put(this, PrefConst.PREFKEY_MAPID, idList);
                        }
                    }

                    if (getCount == 0) getAddress();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getAddress(){

        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+String.valueOf(Commons.CUR_LAT)+","+String.valueOf(Commons.CUR_LNG)+"&key="+"AIzaSyCywnwzsPdTbzzGIbCVehh3WWyy6YP483E";

        Log.d("url=name=service", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                try {
                    Log.d("result==>", json);

                    JSONObject jsonobjec =new JSONObject(json);
                    JSONArray resultarray =jsonobjec.getJSONArray("results");
                    JSONObject oneresult = resultarray.getJSONObject(0);
                    JSONArray address_components=oneresult.getJSONArray("address_components");

                    int length = address_components.length();

                    if (length > 0){

                        JSONObject json_cur_location = address_components.getJSONObject(0);
                        Commons.cur_sub_location = json_cur_location.getString("long_name");

                        JSONObject json_cur_sub_location = address_components.getJSONObject(1);
                        Commons.cur_location  = json_cur_sub_location.getString("long_name");

                        getCount++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(GpsService.this, "Not getting location address", Toast.LENGTH_SHORT).show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RBTApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void initLocationListener() {

        myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (w_bGpsEnabled || w_bNetworkEnabled) {
            tryGetLocation();
        } else {
            setMyLocation(null);
        }
    }

    private void tryGetLocation() {

        w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (w_bNetworkEnabled) {

            Location locationNet = myLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (locationNet != null) {
                setMyLocation(locationNet);
            }
        }
        if (w_bGpsEnabled) {

            Location locationGps = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGps != null) {
                setMyLocation(locationGps);
            }
        }

        if (w_bNetworkEnabled) {

            myLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000,
                    0, m_myLocationListener);
        }
        if (w_bGpsEnabled) {
            myLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    1000,
                    0, m_myLocationListener);
        }
    }

    private void setMyLocation(Location p_location) {

        if (p_location != null) {
            w_fLatitude = p_location.getLatitude();
            w_fLongitude = p_location.getLongitude();
            w_fspeed=p_location.getSpeed();
        }

        System.out.println("Latitude: " + String.valueOf(w_fLatitude) + "Longitude: " );
    }

    LocationListener m_myLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(GpsService.this, "disable", Toast.LENGTH_SHORT).show();
            Log.d("disabale-=====", "disable=========");
        }

        @Override
        public void onLocationChanged(Location location) {

            String log = "pos = " + location.getLatitude() + "," + location.getLongitude() + "," + location.getSpeed() + ":" + new Date().toString();
           // appendLog(log);
            setMyLocation(location);
        }
    };

    //=============== wifi check=====================

    public void   Wifichecked(){
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            // Do whatever
            wifistringth=1;
            cellularstrength=0;
        }else {
            wifistringth=0;
            cellularstrength=1;
        }
    }
}
