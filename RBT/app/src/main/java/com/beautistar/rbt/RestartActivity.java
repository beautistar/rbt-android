package com.beautistar.rbt;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.beautistar.rbt.activity.SplashActivity;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;


/**
 * Created by HugeRain on 4/7/2017.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Commons.g_isAppRunning){

            String room = getIntent().getStringExtra(Constants.KEY_ROOM);

            Intent goIntro = new Intent(this, SplashActivity.class);

            if (room != null)
                goIntro.putExtra(Constants.KEY_ROOM, room);

            startActivity(goIntro);
        }

        finish();

    }


    @Override
    protected void onDestroy(){

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        return true;
    }

    @Override
    public void onClick(View v) {

    }
}
