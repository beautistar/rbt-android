package com.beautistar.rbt.preference;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_MAPID = "mapID";
    public static final String PREFKEY_ALERTID = "alertID";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_USERNAME = "username";
}

