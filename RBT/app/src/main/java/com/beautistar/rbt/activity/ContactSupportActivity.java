package com.beautistar.rbt.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.net.Uri;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beautistar.rbt.R;
import com.beautistar.rbt.base.CommonActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactSupportActivity extends CommonActivity {

    @BindView(R.id.txv_title)
    TextView txv_title;
    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_message) EditText edt_message;

    String name = "", email = "", msg = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_support);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadLayout();
    }

    private void loadLayout() {

        txv_title.setText("Contact Support");

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.lyt_support);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);
                return false;
            }
        });

        name = edt_name.getText().toString();
        email = edt_email.getText().toString();
        msg = edt_message.getText().toString();

    }

    @OnClick(R.id.btn_send) void SendEmail(){
        if (!checkValid()) return;

        String _name = edt_name.getText().toString();
        String _email[] = {edt_email.getText().toString()};
        String _msg = edt_message.getText().toString();
        sendEmail(_name, _email, _msg);

    }

    private void sendEmail(String name, String[] email, String msg){

        Log.d("name==>", name + email + msg);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_TITLE, "title");
        intent.putExtra(Intent.EXTRA_SUBJECT, name);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        //intent.setType("text/plain");
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Send Email"));
        //startActivity(intent);
    }


    private boolean checkValid(){

        if (edt_email.getText().length() == 0){
            showAlertDialog("Please input your email");
            return false;
        } else if (edt_name.getText().length() == 0) {
            showAlertDialog("Please input your name");
            return false;
        }

        return true;
    }

    @OnClick(R.id.imv_back) void gotoMain(){

        finish();
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
