package com.beautistar.rbt.common;


import com.beautistar.rbt.activity.MainActivity;
import com.beautistar.rbt.model.AlertModel;

import java.util.ArrayList;

/**
 * Created by HugeRain on 2/26/2017.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int SPLASH_TIME = 2000;
    public static final int PICK_FROM_GPSSETTINGS = 109;
    public static final String KEY_ROOM = "room";
    public static final String KEY_LOGOUT = "logout";
    public static int service_status = 0;
    public static final int LOCATION_UPDATE_TIME = 5000;
    public static MainActivity mainActivity=null;
    public static ArrayList<AlertModel>  CHANGE_MAP = new ArrayList<>();

    public static String KEY_TERMS = "terms";
    public static String KEY_RESULT = "KEY_RESULT";
    public static String KEY_ORG_PG = "KEY_ORG_PG";

    public static String KEY_POLICY = "policy";
    public static boolean GPS_ENABLE = false;

}
