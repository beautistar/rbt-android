package com.beautistar.rbt.model;

import java.io.Serializable;

/**
 * Created by ITWhiz4U on 10/19/2017.
 */

public class AlertModel implements Serializable {

    int id = 0;
    int userId = 0;
    String fullName = "";


    int alertType = 0;  /*1: police, 2: emergency, 3:fire*/
    String location = "";
    String sub_location = "";
    Double latitude = 0.0;
    Double longitude = 0.0;
    int member = 0;
    String recordTime = "";
    Double distance = 0.0;
    int alertCount = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAlertType() {
        return alertType;
    }

    public void setAlertType(int alertType) {
        this.alertType = alertType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSub_location(){
        return sub_location;
    }

    public void setSub_location(String sub_location){
        this.sub_location = sub_location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getMember() {
        return member;
    }

    public void setMember(int member) {
        this.member = member;
    }

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public int getAlertCount(){return  alertCount;}
}
