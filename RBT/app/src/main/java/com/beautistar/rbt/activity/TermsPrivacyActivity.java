package com.beautistar.rbt.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.beautistar.rbt.R;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Constants;

import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsPrivacyActivity extends CommonActivity {

    @BindView(R.id.txv_content) TextView txv_content;
    @BindView(R.id.txv_title) TextView txv_title;

    String result;
    String result_code = "";
    String org_page = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_privacy);

        ButterKnife.bind(this);
        result_code = (String) getIntent().getExtras().getString(Constants.KEY_RESULT, "");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadLayout();
    }

    private void loadLayout() {

        if (result_code.equals("TERMS")) {

            txv_title.setText("Terms of Use");
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_terms);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }

        } else if (result_code.equals("POLICY")){

            txv_title.setText("Privacy Statement");
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_privacy);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }
        txv_content.setText(result.toString());
    }


    @OnClick(R.id.imv_back) void gotoSignUp(){
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
