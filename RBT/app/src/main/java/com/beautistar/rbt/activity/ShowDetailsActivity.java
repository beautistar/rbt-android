package com.beautistar.rbt.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.model.AlertModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowDetailsActivity extends CommonActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

    public static ShowDetailsActivity instance;
    private GoogleMap mMap;
    ArrayList<AlertModel> alertMap = new ArrayList<>();
    @BindView(R.id.txv_mapPage)
    TextView txv_mapPage;
    int count = 0;

    ArrayList<MarkerOptions> markerOptionses = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        ButterKnife.bind(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.show_map);
        mapFragment.getMapAsync(this);

        loadLayout();

        instance = this;
    }

    private void loadLayout() {

        txv_mapPage.setTextColor(getResources().getColor(R.color.login_btn_color));

        if (Constants.CHANGE_MAP != null && Constants.CHANGE_MAP.size() > 0) {
            alertMap = Constants.CHANGE_MAP;
        } else findAlertMap();
    }

    private void findAlertMap() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FIND_ALERT_MAP;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseFindAlertMap(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));

                } catch (Exception e) {
                }
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseFindAlertMap(String json) {

        Log.d("responseMapList==>", json);

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS) {

                JSONArray alertList = response.getJSONArray(ReqConst.RES_ALERT_LIST);

                for (int i = 0; i < alertList.length(); i++) {

                    JSONObject jsonAlert = (JSONObject) alertList.get(i);
                    AlertModel alertModel = new AlertModel();

                    alertModel.setId(jsonAlert.getInt(ReqConst.RES_ALERT_ID));
                    alertModel.setFullName(jsonAlert.getString(ReqConst.RES_FULL_NAME));
                    alertModel.setAlertType(jsonAlert.getInt(ReqConst.RES_ALERT_TYPE));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_LOCATION));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_SUB_LOCATION));
                    alertModel.setLatitude(jsonAlert.getDouble(ReqConst.RES_LATITUDE));
                    alertModel.setLongitude(jsonAlert.getDouble(ReqConst.RES_LONGITUDE));
                    alertModel.setMember(jsonAlert.getInt(ReqConst.RES_MEMBERS));
                    alertModel.setRecordTime(jsonAlert.getString(ReqConst.RES_RECORD_TIME));
                    alertModel.setDistance(jsonAlert.getDouble(ReqConst.RES_DISTANCE));

                    alertMap.add(alertModel);
                }

                addMarkers();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addMarkers() {

        alertMap = Constants.CHANGE_MAP;

        for (int i = 0; i < alertMap.size(); i++) {

            Double lat = (alertMap.get(i).getLatitude());
            Double lng = (alertMap.get(i).getLongitude());

            Log.d("latlnag=+++++=", String.valueOf(lat) + "===" + String.valueOf(lng));
            final LatLng latLng = new LatLng(lat, lng);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            int alertType = alertMap.get(i).getAlertType();
            if (alertType == 1)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police_marker));
            else if (alertType == 2)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_emer_marker));
            else if (alertType == 3)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_fire_marker));

            //markerOptions.title(alertMap.get(i).getCompanyname());
            // markerOptions.title(Constants.companymodels.get(i).getCompanyname());
            markerOptionses.add(markerOptions);
            mMap.addMarker(markerOptions);
        }
    }

    @OnClick(R.id.imv_homePage) void gotoMain(){

        startActivity(new Intent(ShowDetailsActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.imv_alertPage) void gotoAlert(){

        startActivity(new Intent(ShowDetailsActivity.this, AlertShowActivity.class));
        overridePendingTransition(0,0);
        finish();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);

        if (mMap != null) {

            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                @Override
                public void onMyLocationChange(Location arg0) {

                    mMap.clear();
                    LatLng latLng= new LatLng(arg0.getLatitude(), arg0.getLongitude());
                    MarkerOptions options = new MarkerOptions().position(latLng).title("Current My Loacation");
                    //options.icon(BitmapDescriptorFactory.defaultMarker());
                    options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_car));
                    mMap.addMarker(options);

                    if (count == 0) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10.5f));
                    }

                    addMarkers();
                    count++;
                }
            });
        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onBackPressed() {
        gotoMain();
    }

}
