package com.beautistar.rbt.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beautistar.rbt.R;
import com.beautistar.rbt.activity.AlertShowActivity;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.model.AlertModel;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.preference.Preference;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ITWhiz4U on 10/19/2017.
 */

public class AlertShowListViewAdapter extends BaseAdapter {

    AlertShowActivity _activity;
    ArrayList<AlertModel> _allAlert = new ArrayList<>();
    String alertID = "";
    public AlertShowListViewAdapter(AlertShowActivity activity, ArrayList<AlertModel> alerts){

        _activity = activity;
        _allAlert = alerts;

    }
    @Override
    public int getCount() {
        return _allAlert.size();
    }

    @Override
    public Object getItem(int position) {
        return _allAlert.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AlertHolder holder;

        if (convertView == null){

            holder = new AlertHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_alert_list, parent, false);

            holder.imv_alertType = (ImageView)convertView.findViewById(R.id.imv_alertType);
            holder.imv_stopAlert = (ImageView)convertView.findViewById(R.id.imv_clearAlert);
            holder.txv_alertSuburb = (TextView)convertView.findViewById(R.id.txv_alertSuburb);
            holder.txv_alertLocation = (TextView)convertView.findViewById(R.id.txv_alertLocation);
            holder.txv_time = (TextView)convertView.findViewById(R.id.txv_time);
            holder.txv_rep_num = (TextView)convertView.findViewById(R.id.txv_rep_num);

            convertView.setTag(holder);

        } else holder = (AlertHolder)convertView.getTag();

        final AlertModel alert = (AlertModel)_allAlert.get(position);

        int alertType = alert.getAlertType();

        if (alertType == 1) Glide.with(_activity).load(R.drawable.ic_police_large).placeholder(R.drawable.bg_non_profile).into(holder.imv_alertType);
        else if (alertType == 2) Glide.with(_activity).load(R.drawable.ic_emer_large).placeholder(R.drawable.bg_non_profile).into(holder.imv_alertType);
        else if (alertType ==3) Glide.with(_activity).load(R.drawable.ic_fire_large).placeholder(R.drawable.bg_non_profile).into(holder.imv_alertType);

        holder.txv_alertLocation.setText(alert.getLocation());
        holder.txv_alertSuburb.setText(alert.getSub_location());

        String current_time = "";
        String dateStr = alert.getRecordTime();/*"12:08:59 AM";*/

        //String time_ = "2017-11-12 11:10:44";
        String time_ = dateStr.split(" ")[1];

        String hour_ = time_.split(":")[0];
        String min = time_.split(":")[1];
        if (Integer.parseInt(hour_) > 12){

            String hour_spilt = String.valueOf(Integer.parseInt(hour_) - 12);
            current_time = hour_spilt +":" + min+ " " + "PM";

        } else {

            current_time = hour_ +":"+ min+  " " + "AM";
        }
        SimpleDateFormat df = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = df.parse(current_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);

        String tt = formattedDate.split(" ")[0];

        if (Integer.parseInt(tt.split(":")[0]) > 12){

            String tt_split = String.valueOf(Integer.parseInt(tt.split(":")[0]) - 12);
            holder.txv_time.setText(tt_split +":" + tt.split(":")[1]+ " " + "PM");

        } else {

            holder.txv_time.setText(String.valueOf(formattedDate));
        }

        //holder.txv_time.setText(dateStr);

        holder.txv_rep_num.setText(String.valueOf(alert.getMember()));

        holder.imv_stopAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (alertID.length() == 0) {
                    Preference.getInstance().put(_activity, PrefConst.PREFKEY_ALERTID, String.valueOf(alert.getId()));
                }
                else {
                    alertID = alertID + "," + alert.getId();
                    Preference.getInstance().put(_activity, PrefConst.PREFKEY_ALERTID, alertID);
                }
                removeAlert(alert);
            }
        });

        return convertView;
    }

    private void removeAlert(AlertModel alert){

        _allAlert.remove(alert);
        notifyDataSetChanged();
    }

    public static class AlertHolder{

        ImageView imv_alertType;
        ImageView imv_stopAlert;
        TextView txv_alertSuburb;
        TextView txv_alertLocation;
        TextView txv_time;
        TextView txv_rep_num;

    }
}
