package com.beautistar.rbt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.adapter.AlertShowListViewAdapter;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.model.AlertModel;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.leolin.shortcutbadger.ShortcutBadger;

public class AlertShowActivity extends CommonActivity {

    @BindView(R.id.lst_alert_show) ListView lst_alertShow;
    AlertShowListViewAdapter _adapter;

    ArrayList<AlertModel> _alerts = new ArrayList<>();
    @BindView(R.id.txv_alertPage)
    TextView txv_alertPage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_show);

        ButterKnife.bind(this);
        getAlertList();
        loadLayout();
    }

    private void getAlertList(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_ALERT_LIST;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetList(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));

                } catch (Exception e) {}
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetList(String json){

        Log.d("json==>", json);

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code ==  ReqConst.CODE_SUCCESS){

                JSONArray alertList = response.getJSONArray(ReqConst.RES_ALERT_LIST);

                _alerts.clear();

                for (int i = 0; i < alertList.length (); i++ ){

                    JSONObject jsonAlert = (JSONObject) alertList.get(i);
                    AlertModel alertModel = new AlertModel();

                    alertModel.setId(jsonAlert.getInt(ReqConst.RES_ALERT_ID));
                    //compare the alert ID
                    String alertID = Preference.getInstance().getValue(this, PrefConst.PREFKEY_ALERTID, "");
                    if (alertID.contains(String.valueOf(alertModel.getId()))) continue;

                    alertModel.setFullName(jsonAlert.getString(ReqConst.RES_FULL_NAME));
                    alertModel.setAlertType(jsonAlert.getInt(ReqConst.RES_ALERT_TYPE));
                    alertModel.setLocation(jsonAlert.getString(ReqConst.RES_LOCATION));
                    alertModel.setSub_location(jsonAlert.getString(ReqConst.RES_SUB_LOCATION));
                    alertModel.setLatitude(jsonAlert.getDouble(ReqConst.RES_LATITUDE));
                    alertModel.setLongitude(jsonAlert.getDouble(ReqConst.RES_LONGITUDE));
                    alertModel.setMember(jsonAlert.getInt(ReqConst.RES_MEMBERS));
                    alertModel.setRecordTime(jsonAlert.getString(ReqConst.RES_RECORD_TIME));
                    alertModel.setDistance(jsonAlert.getDouble(ReqConst.RES_DISTANCE));

                    _alerts.add(alertModel);
                }
                _adapter = new AlertShowListViewAdapter(this, _alerts);
                lst_alertShow.setAdapter(_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        txv_alertPage.setTextColor(getResources().getColor(R.color.login_btn_color));
    }

    @OnClick(R.id.imv_homePage) void gotoHome(){

        startActivity(new Intent(AlertShowActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.imv_mapPage) void gotoMap(){

        startActivity(new Intent(AlertShowActivity.this, ShowDetailsActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoHome();
    }
}
