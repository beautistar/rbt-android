package com.beautistar.rbt.common;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;

import com.beautistar.rbt.model.UserModel;

/**
 * Created by HGS on 12/11/2015.
 */
public class Commons {

    public static double CUR_LAT = 0;
    public static double CUR_LNG = 0;
    public static String STR_LAT = "";
    public static String STR_LNG = "";

    public static String cur_location = "";
    public static String cur_sub_location = "";

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static int SCREEN_WIDTH = 0;

    public static int SCREEN_HEIGHT = 0;

    public static int STATUSBAR_HEIGHT = 0;

    public static int SHARE_OPEN_COUNT = 3;

    public static Handler g_handler = null;

    public static String g_deviceId = null;

    public static String g_appPath = "";

    public static String g_appVersion = "1.0";

    public static UserModel g_user = new UserModel();


/*    public static GroupChattingActivity g_chattingActivity = null;

    public static ConnectionMgrService g_xmppService = null;*/



    public static int g_badgCount = 0;

    public static int GetPixelValueFromDp(Context context, float dp_value) {

        int pxValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp_value, context.getResources()
                        .getDisplayMetrics());

        return pxValue;
    }


    public static int addrToIdx(String addr) {
        int pos = addr.indexOf("@");
        return Integer.valueOf(addr.substring(0, pos)).intValue();
    }

    public static String fileExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf(".") == -1) {
            return url;
        } else {
            String ext = url.substring(url.lastIndexOf(".") );
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

}
