package com.beautistar.rbt.common;

import java.util.logging.Handler;

public class ReqConst {

    //public static final String SERVER_ADDR = "http://54.68.180.131";
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;


    public static final String SERVER_ADDR = "http://18.216.24.138";


    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    //Request value
    public static final String REQ_LOGIN = "login";
    public static final String REQ_REGISTER = "register";
    public static final String REQ_FB_REGISTER = "fbLogin";
    public static final String REQ_SET_ALERT = "setAlert";
    public static final String REQ_GET_ALERT_LIST = "getAlertList";
    public static final String REQ_FIND_ALERT_MAP = "findAlertMap";


    //Request Params

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_FB_ID = "fb_id";
    public static final String PARAM_FULL_NAME = "full_name";
    public static final String PARAM_CUR_LAT = "cur_lat";
    public static final String PARAM_CUR_LNG = "cur_lng";
    public static final String PARAM_TYPE ="type";
    public static final String PARAM_LOCATION = "location";
    public static final String PARAM_SUB_LOCATION = "sub_location";

    //response value

    //getAlertList
    public static final String RES_USER_INFO = "user_info";
    public static final String RES_ALERT_LIST = "alert_list";
    public static final String RES_ALERT_ID = "id";
    public static final String RES_USER_ID = "user_id";
    public static final String RES_FULL_NAME = "full_name";
    public static final String RES_ALERT_TYPE = "alert_type";
    public static final String RES_LOCATION = "location";
    public static final String RES_SUB_LOCATION = "sub_location";
    public static final String RES_LATITUDE = "latitude";
    public static final String RES_LONGITUDE = "longitude";
    public static final String RES_MEMBERS = "members";
    public static final String RES_RECORD_TIME = "record_time";
    public static final String RES_DISTANCE = "distance";

    public static final String RES_CODE = "result_code";
    public static final String RES_MSG = "msg";
    public static final String RES_ALERT_COUNT = "alert_count";

/*
    101 : Already registered.
    102 : Unregistered email or phone number
    105: Failed to upload file*/

    public static final int CODE_SUCCESS = 0;
    public static final int CODE_EXIST_USER = 101;
    public static final int CODE_UNREGISTER = 102;
    public static final int CODE_FAILED = 105;



}
