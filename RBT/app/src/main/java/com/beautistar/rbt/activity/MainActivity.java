package com.beautistar.rbt.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.preference.Preference;
import com.beautistar.rbt.utils.GpsService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.beautistar.rbt.R.drawable.alert;
import static com.beautistar.rbt.common.Commons.cur_sub_location;

public class MainActivity extends CommonActivity implements OnMapReadyCallback {

    Location location = null;
    public LocationManager myLocationManager;
    LocationManager _locationManager;
    String mprovider;

    boolean gps_enabled = false;
    boolean network_enabled = false;

    @BindView(R.id.txv_alertPage) TextView txv_alertPage;
    @BindView(R.id.txv_homePage) TextView txv_homePage;
    @BindView(R.id.txv_mapPage) TextView txv_mapPage;
    @BindView(R.id.imv_menu) ImageView imv_menu;

    GoogleMap mMap;
    double latitude=0, longitude=0;
    int initiallocation=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.show_map_main);
        mapFragment.getMapAsync(this);

        if (checkLocationEnabled())
        startGpsService();
        lodLayout();

        if(Commons.CUR_LNG!=0){
            getAddress();
        }
    }

    public void startGpsService(){

        if(Constants.service_status == 0){
            Intent serviceIntent = new Intent(MainActivity.this, GpsService.class);
            startService(serviceIntent);
        }
    }

    public boolean checkLocationEnabled()
    {
        _locationManager = (LocationManager) _context.getSystemService( Context.LOCATION_SERVICE );

        if ( !_locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) /*|| !_locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)*/ ) {
            buildAlertMessageNoGps();
            return false;
        }

        return true;
    }
    private void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,  final int id) {

                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), Constants.PICK_FROM_GPSSETTINGS);
                        Constants.GPS_ENABLE = true;
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {

                        Constants.GPS_ENABLE = false;
                        showAlertDialog("You can't service the location");
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void lodLayout() {

        txv_homePage.setTextColor(getResources().getColor(R.color.login_btn_color));

    }

    private void getAddress(){

        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+String.valueOf(Commons.CUR_LAT)+","+String.valueOf(Commons.CUR_LNG)+"&key="+"AIzaSyCywnwzsPdTbzzGIbCVehh3WWyy6YP483E";

        Log.d("url=name=", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                try {

                    Log.d("result==>", json);

                    JSONObject jsonobjec =new JSONObject(json);
                    JSONArray resultarray =jsonobjec.getJSONArray("results");
                    JSONObject oneresult = resultarray.getJSONObject(0);
                    JSONArray address_components=oneresult.getJSONArray("address_components");

                    int length = address_components.length();

                    if (length > 0){

                        JSONObject json_cur_location = address_components.getJSONObject(0);
                        Commons.cur_sub_location = json_cur_location.getString("long_name");

                        JSONObject json_cur_sub_location = address_components.getJSONObject(1);
                        Commons.cur_location  = json_cur_sub_location.getString("long_name");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                showToast( "Not getting location address");
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RBTApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void setAlert(final int type) {

        Log.d("logn===>", String.valueOf(Commons.CUR_LAT +"   ;;;;" + Commons.CUR_LNG));

        showProgress();

        if (Commons.cur_location.length() == 0 || Commons.cur_sub_location.length() == 0 || Commons.cur_sub_location.equals("") || Commons.cur_location.equals("") || Commons.STR_LAT.length() == 0 || Commons.STR_LNG.length() == 0) {
            closeProgress();
            showToast("Searching address..." + "Try again" );
            getAddress();
            return;}

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SET_ALERT;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap<>();
                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.getUser_id()));
                    params.put(ReqConst.PARAM_FULL_NAME, Commons.g_user.getFull_name());
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));
                    params.put(ReqConst.PARAM_TYPE, String.valueOf(type));
                    params.put(ReqConst.PARAM_LOCATION, Commons.cur_location);
                    params.put(ReqConst.PARAM_SUB_LOCATION, Commons.cur_sub_location);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseResponse(String json) {
        Log.d("resSetAlert==>", json);
        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                //showToast(response.getString(ReqConst.RES_MSG));
                showAlertMembers();

            } else showAlertDialog(response.getString(ReqConst.RES_MSG));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.imv_alertPolice) void police_setAlert(){

        clickSound();
        if (!checkLocationEnabled()) return;
        setAlert(1);
    }

    @OnClick(R.id.imv_alertEmergency) void emergency_setAlert(){
        clickSound();
        if (!checkLocationEnabled()) return;
        setAlert(2);
    }

    @OnClick(R.id.imv_alertFire) void fire_setAlert(){
        clickSound();
        if (!checkLocationEnabled()) return;
        setAlert(3);
    }

    @OnClick(R.id.imv_alertPage) void gotoAlert(){

        Intent intent = new Intent(this, AlertShowActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);

    }

    @OnClick(R.id.imv_mapPage) void gotoMaps(){

        Intent intent = new Intent(this, ShowDetailsActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);

    }

    public void showAlertMembers() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        dialog.show();

        new CountDownTimer(2500, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {

                dialog.dismiss();
            }

        }.start();

    }

    private void clickSound(){

        try {
            //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri notification = Uri.parse("android.resource://com.beautistar.rbt/raw/click_sound");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        onExit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        if (mMap != null) {
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                @Override
                public void onMyLocationChange(Location arg0) {
                    if (initiallocation==0) {
                       Commons.CUR_LAT = arg0.getLatitude();
                       Commons.CUR_LNG = arg0.getLongitude();

                       Log.d("Longitude==>", String.valueOf(Commons.CUR_LNG));
                       Log.d("Latitude==>", String.valueOf(Commons.CUR_LAT));
                       initiallocation=1;
                    }
                }
            });
        }

    }

    @OnClick(R.id.imv_menu) void showMenu(){

            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(MainActivity.this, imv_menu);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    switch (item.getItemId()) {

           /* case R.id.tb_setting:
                return true;*/
                        case R.id.popup_faq:

                            startActivity(new Intent(MainActivity.this, FaqHelpActivity.class));
                            overridePendingTransition(0,0);
                            return true;

                        case R.id.popup_privacy:

                            Intent intent = new Intent(MainActivity.this, TermsPrivacyActivity.class);
                            intent.putExtra(Constants.KEY_RESULT, "POLICY");
                            startActivity(intent);

                            overridePendingTransition(0,0);
                            return true;

                        case R.id.popup_terms:

                            Intent intent_terms = new Intent(MainActivity.this, TermsPrivacyActivity.class);
                            intent_terms.putExtra(Constants.KEY_RESULT, "TERMS");
                            startActivity(intent_terms);

                            overridePendingTransition(0,0);
                            return true;

                        case R.id.popup_contact:

                            startActivity(new Intent(MainActivity.this, ContactSupportActivity.class));
                            overridePendingTransition(0,0);

                            return true;
                    }

                    Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                    return true;

                }
            });

            popup.show();//showing popup menu
    }
}

