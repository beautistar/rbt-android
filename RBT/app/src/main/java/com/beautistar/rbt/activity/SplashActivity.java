package com.beautistar.rbt.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.preference.PrefConst;
import com.beautistar.rbt.utils.GpsService;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends CommonActivity {

    String _email = "";
    String _password = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Commons.g_isAppRunning = true;
        startGpsService();
        gotoMain();
    }

    public void startGpsService(){

        if(Constants.service_status == 0){
            Intent serviceIntent = new Intent(this, GpsService.class);
            startService(serviceIntent);
        }
    }

    public void gotoMain(){

        new Handler().postDelayed(new Runnable(){

            @Override
            public void run() {
                loadLayout();
            }
        }, Constants.SPLASH_TIME);
    }

    private void loadLayout(){

        _email = EasyPreference.with(this).getString( PrefConst.PREFKEY_USEREMAIL, "");
        _password = EasyPreference.with(this).getString(PrefConst.PREFKEY_USERPWD, "");

        if ( _email.length()>0 && _password.length() > 0 ) {
            getUserInfo();

        } else {

            Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
            startActivity(intent);
            finish();
        }
    }

    //get User info
    private void getUserInfo(){

        //showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;
        Log.d("url==>", url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                try {
                    params.put(ReqConst.PARAM_EMAIL, _email);
                    params.put(ReqConst.PARAM_PASSWORD, _password);
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }
    //parse from server to get user info.
    private void parseLogin(String json){

        //closeProgress();

        Log.d("Response==>", json);

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                JSONObject user_info = response.getJSONObject(ReqConst.RES_USER_INFO);

                Commons.g_user.setUser_id(user_info.getInt(ReqConst.RES_USER_ID));
                Commons.g_user.setFull_name(user_info.getString(ReqConst.RES_FULL_NAME));

                startActivity(new Intent(this, MainActivity.class));
                overridePendingTransition(0,0);
                finish();

            } else showAlertDialog(response.getString(ReqConst.RES_MSG));

        } catch (JSONException e) {
            e.printStackTrace();
            closeProgress();
        }
    }
}
