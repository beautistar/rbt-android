package com.beautistar.rbt.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.beautistar.rbt.R;
import com.beautistar.rbt.RBTApplication;
import com.beautistar.rbt.base.CommonActivity;
import com.beautistar.rbt.common.Commons;
import com.beautistar.rbt.common.Constants;
import com.beautistar.rbt.common.ReqConst;
import com.beautistar.rbt.preference.PrefConst;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends CommonActivity {

    @BindView(R.id.edt_firstName) EditText edt_firstName;
    @BindView(R.id.edt_lastName) EditText edt_lastName;
    //@BindView(R.id.edt_userName) EditText edt_userName;
    @BindView(R.id.edt_userPass) EditText edt_userPass;
    @BindView(R.id.edt_re_pass) EditText edt_rePass;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.btn_signup) Button btn_signup;
    @BindView(R.id.chx_check) CheckBox chx_check;
    @BindView(R.id.btn_login) Button btn_login;

    @BindView(R.id.txv_login_facebook) TextView facebook_login;

    // facebook login
    public static CallbackManager callbackManager;
    private String FEmail, FullName, Name,Firstname, Lastname,Gender, Id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        ButterKnife.bind(this);

        loadLayout();

        checkAllPermission();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        try
        {
            PackageInfo info = getPackageManager().getPackageInfo("com.beautistar.rbt", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                Log.i("KeyHash::", Base64.encodeToString(md.digest(), Base64.DEFAULT));//will give developer key hash

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {}
    }

    int MY_PEQUEST_CODE = 123;
    String[] PERMISSIONS = { Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION };

    /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {}
    }

    private void loadLayout() {

        Typeface font = Typeface.createFromAsset(getAssets(), "font/Gobold Regular.otf");
        btn_signup.setTypeface(font);

        TextView txv_create_title = (TextView)findViewById(R.id.txv_create_title);
        txv_create_title.setTypeface(font);
        btn_login.setTypeface(font);
        facebook_login.setTypeface(font);

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.signup);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_firstName.getWindowToken(),0);
                return false;
            }
        });
    }

    @OnClick(R.id.btn_login) void gotoLoginPage(){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    //Sign up
    @OnClick(R.id.btn_signup) void gotoSignUp(){

        if (!checkValid() && Commons.CUR_LAT != 0.0 && Commons.CUR_LNG != 0.0) {

            showToast("Try again.");
            return;
        }

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTER;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResister(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap();
                try {
                    params.put(ReqConst.PARAM_FULL_NAME, (edt_firstName.getText().toString().trim() + edt_lastName.getText().toString().trim()));
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString().trim());
                    params.put(ReqConst.PARAM_PASSWORD, edt_userPass.getText().toString());
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));

                }catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(request, url);
    }

    //parse sign up
    private void parseResister(String json){

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                Commons.g_user.setUser_id(response.getInt(ReqConst.RES_USER_ID));
                Commons.g_user.setFull_name(edt_firstName.getText().toString() + edt_lastName.getText().toString());

                EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL, edt_email.getText().toString().trim()).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, edt_userPass.getText().toString().trim()).save();

                startActivity(new Intent(this, MainActivity.class));
                overridePendingTransition(0,0);
                finish();

            } else showAlertDialog(response.getString(ReqConst.RES_MSG));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //goto facebook login.
    @OnClick(R.id.txv_login_facebook) void loginWithFb(){
        loginWithFaceBook();
    }

    public void loginWithFaceBook() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                Log.d("accessToken=====>", String.valueOf(accessToken));

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.v("LoginActivity Response ", response.toString());

                        try {

                            Name = object.getString("name");
                            Name.replace(" ", "");
                            Id = object.getString("id");
                            Firstname = object.getString("first_name");
                            //FullName = object.getString("full_name");
                            Lastname = object.getString("last_name");
                            //Gender = object.getString("gender");

                            //Password= object.getString("password");
                            FEmail = object.getString("email");
                            /*Image_url = "http://graph.facebook.com/(Id)/picture?type=large";
                            Image_url = URLEncoder.encode(Image_url);*/

                            Log.d("Email = ", " " + FEmail);
                            Log.d("firstName======",Firstname);
                            Log.d("lastName======",Lastname);
                            Log.d("id======",Id);

                            //photourl = "http://graph.facebook.com/"+Id+"/picture?type=large";
                            FullName = Firstname + Lastname;
                            SocialLogin(Id, FullName);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    //==================================Face book Login End======================================

    public void SocialLogin(final String fb_id, final String FullName){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FB_REGISTER ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));

                parseEditResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    String password = "123456";
                    //params.put("id",String.valueOf(Commons.g_user.get_idx()));

                    params.put(ReqConst.PARAM_FB_ID,fb_id);
                    params.put(ReqConst.PARAM_FULL_NAME, FullName);
                    params.put(ReqConst.PARAM_CUR_LAT, String.valueOf(Commons.CUR_LAT));
                    params.put(ReqConst.PARAM_CUR_LNG, String.valueOf(Commons.CUR_LNG));


                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RBTApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseEditResponse(String json){

        Log.d("facebookres==>", json);

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (ReqConst.CODE_SUCCESS == result_code){

                JSONObject userInfo = response.getJSONObject(ReqConst.RES_USER_INFO);

                //showToast(userInfo.getString(ReqConst.RES_MSG));
                int user_id = userInfo.getInt(ReqConst.RES_USER_ID);

                Commons.g_user.setUser_id(user_id);
                Commons.g_user.setFull_name(userInfo.getString(ReqConst.RES_FULL_NAME));

                //EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL, FEmail).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, edt_userPass.getText().toString().trim()).save();

                startActivity(new Intent(this, MainActivity.class));
                overridePendingTransition(0,0);
                finish();

            } else {
                showAlertDialog(getString(R.string.error));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txv_terms) void gotoTerms(){

        chx_check.setChecked(true);

        Intent intent = new Intent(this, TermsPrivacyActivity.class);
        intent.putExtra(Constants.KEY_RESULT, "TERMS");
        startActivity(intent);
    }

    @OnClick(R.id.txv_privacy) void gotoPolicy(){

        chx_check.setChecked(true);

        Intent intent = new Intent(this, TermsPrivacyActivity.class);
        intent.putExtra(Constants.KEY_RESULT, "POLICY");
        startActivity(intent);
    }

    private boolean checkValid(){

        if (edt_firstName.getText().toString().length() == 0){
            showAlertDialog("Please input first name");
            return false;
        } else if (edt_lastName.getText().length() == 0){
            showAlertDialog("Please input last name");
            return false;
        } else if (edt_userPass.getText().length() == 0){
            showAlertDialog("Please input password");
            return false;
        } else if (edt_rePass.getText().length() == 0){
            showAlertDialog("Please input re-enter password");
            return false;
        } else if (!edt_userPass.getText().toString().equals(edt_rePass.getText().toString())){
            showAlertDialog("Please confirm your password");
            return false;
        } else if (!chx_check.isChecked()){
            showAlertDialog("Please check the Terms and Privacy Statement.");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
