package com.beautistar.rbt.model;

import java.io.Serializable;

/**
 * Created by ITWhiz4U on 10/23/2017.
 */

public class UserModel implements Serializable {

    int user_id = 0;
    String full_name = "";

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
